package main

import (
    "bufio"
    "fmt"
    "math/rand"
	"os"
    "strconv"
    "time"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	num := rand.Intn(1000)
	
	guesses := 0
	
	scanner := bufio.NewScanner(os.Stdin)
	
	for {
		fmt.Print("Guess a number between 0 and 1000: ")
		scanner.Scan()
		guess, err := strconv.Atoi(scanner.Text())
		
		if err != nil {
			fmt.Println("Error reading input.")
			fmt.Println(err)
			os.Exit(1)
		}
		
		if guess >= 0 && guess <= 1000 {
			guesses++
			
			if guess == num {
				fmt.Println("Correct!")
				fmt.Printf("Total guesses: %d\n", guesses)
				break
			} else if guess < num {
				fmt.Println("Too low.")
			} else {
				fmt.Println("Too high.")
			}
		} else {
			fmt.Println("Invalid guess.")
		}
	}
}