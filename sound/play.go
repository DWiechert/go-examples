package main

import (
	"log"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

// Copied from - https://github.com/faiface/beep/wiki/Hello,-Beep!
func main() {
	// Read in file
	f, err := os.Open("Computer_Music_All-stars_-_Energy_Fix.mp3")
	if err != nil {
		log.Fatal(err)
	}
	
	// Initialize streamer and format
	streamer, format, err := mp3.Decode(f)
	if err != nil {
		log.Fatal(err)
	}
	// Auto-close streamer when done
	defer streamer.Close()
	
	// Initialize speaker system
	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
	
	// Play streamer on speaker
	// Create callback so program waits for song to finish
	done := make(chan bool)
	speaker.Play(beep.Seq(streamer, beep.Callback(func() {
		done <- true
	})))

	<-done
}