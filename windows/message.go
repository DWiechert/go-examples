package main

import (
	"fmt"
	"syscall"
	"unsafe"
)

// Go Example:
// 	- https://github.com/golang/go/wiki/WindowsDLLs
// Go Docs:
// 	- https://golang.org/src/syscall/dll_windows.go
// 	- https://golang.org/src/syscall/syscall_windows.go
// Windows API:
//	- https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-messageboxw
func main() {
	var mod = syscall.NewLazyDLL("user32.dll")
	var proc = mod.NewProc("MessageBoxW")
	var MB_YESNOCANCEL = 0x00000003
	var MB_ICONEXCLAMATION = 0x00000030
	var MB_SETFOREGROUND = 0x00010000
	var messageNum = MB_YESNOCANCEL | MB_ICONEXCLAMATION | MB_SETFOREGROUND

	ret, _, _ := proc.Call(0,
		uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr("This test is Done."))),
		uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr("Done Title"))),
		uintptr(messageNum))
	fmt.Printf("Return: %d\n", ret)

}